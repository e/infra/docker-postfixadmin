#!/bin/bash


ENCRYPTION=${ENCRYPTION:-"dovecot:SHA512-CRYPT"}

# launch default image's docker-entrypoint
/usr/local/bin/docker-entrypoint.sh apache2


#  pfa /e/ customization
sed -i "$ i \$CONF['configured'] = true;"  /postfixadmin/config.local.php
sed -i "$ i \$CONF['encrypt'] = '${ENCRYPTION}';"  /postfixadmin/config.local.php
sed -i "$ i \$CONF['dovecotpw'] = '/usr/bin/doveadm pw';"  /postfixadmin/config.local.php

sed -i "$ i \$CONF['smtp_sendmail_tls'] = 'YES';"  /postfixadmin/config.local.php
sed -i "$ i \$CONF['domain_path'] = 'YES';"  /postfixadmin/config.local.php
#sed -i "$ i \$CONF['domain_in_mailbox'] = 'NO';"  /postfixadmin/config.local.php
#sed -i "$ i \$CONF['fetchmail'] = 'YES';"  /postfixadmin/config.local.php
#sed -i "$ i \$CONF['sendmail'] = 'YES';"  /postfixadmin/config.local.php

sed -i "$ i \$CONF['password_validation'] = array('length_check' => function(\$password) { if (strlen(trim(\$password)) < 5) { return 'password_too_short'; } },);"  /postfixadmin/config.local.php

sed -i "$ i \$CONF['admin_email'] = 'drive@${DOMAIN}';"  /postfixadmin/config.local.php
sed -i "$ i \$CONF['admin_smtp_password'] = '${DRIVE_SMTP_PASSWORD}';"  /postfixadmin/config.local.php
sed -i "$ i \$CONF['footer_text'] = 'Return to ${DOMAIN}';"  /postfixadmin/config.local.php
sed -i "$ i \$CONF['footer_link'] = 'http://${DOMAIN}';"  /postfixadmin/config.local.php


# updating pfexec user's password with the one in .env file
echo "pfexec:$POSTFIXADMIN_SSH_PASSWORD" | chpasswd

# launch ssh server
/etc/init.d/ssh start

# launch apache
apache2-foreground