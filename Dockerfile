FROM postfixadmin:3.3.5-apache
LABEL maintainer="Florent VINCENT <diroots@e.email>"

# /e/ customizations
# dependancies
RUN set -eux; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
        openssh-server \
        sudo \
        dovecot-core \
    ; \
    rm -rf /var/lib/apt/lists/*

# script really deleting mailbox files when a user delete his account
COPY postfixadmin-mailbox-postdeletion.sh /usr/local/bin/
COPY run.sh /usr/local/bin/

# fix perms
RUN chmod +x /usr/local/bin/postfixadmin-mailbox-postdeletion.sh
RUN chmod +x /usr/local/bin/run.sh

# user dedicated to ssh actions
RUN adduser --shell /bin/bash pfexec
RUN adduser pfexec www-data

# giving pfexec user the ability to sudo and restrict him to only use the added script
RUN echo "" >> /etc/sudoers 
RUN echo "#pfexec single command permission" >> /etc/sudoers 
RUN echo "pfexec ALL=(root) NOPASSWD: /usr/local/bin/postfixadmin-mailbox-postdeletion.sh" >> /etc/sudoers

# create symbolic link to be backward compatible with our scripts calling the postfixadmincli tool.
# postfixadmin is now in /var/www/html and not anymore in /postfixadmin
RUN ln -s /var/www/html /postfixadmin

ENTRYPOINT ["/usr/local/bin/run.sh"]
CMD ["apache2-foreground"]